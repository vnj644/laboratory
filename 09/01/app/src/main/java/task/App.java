package task;

// multiplyMatrix
// sumMatrix
// deductionMatrix

class App{
  public static void main(String[] args) {
    int[][] matrix1 = {{4, 5, 2}, {1, 7, 8}, {8, 6, 9}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}, {1, 5, 0}};
    Operations operations = new Operations();
    int[][] endMass = operations.multiplyMatrix(matrix1, matrix2);
    for (int k = 0; k < endMass.length;k++ ) {
      for (int i = 0;i < endMass[0].length ;i++ ) {
        System.out.print(endMass[k][i] + " ");
      }
      System.out.println();
    }
  }
}

