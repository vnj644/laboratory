package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void matrixMultiplyTest_2x3() {
    int[][] matrix1 = {{4, 5}, {1, 7}, {8, 6}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}};
    int[][] finalMatrix = {{38, 27, 55}, {44, 24, 54}, {52, 42, 82}};
    assertArrayEquals(finalMatrix, Operations.multiplyMatrix(matrix1, matrix2));
  }

  @Test void matrixMultiplyTest_3x4() {
    int[][] matrix1 = {{4, 5, 4}, {1, 7, 6}, {8, 6, 7}, {1, 2, 0}};
    int[][] matrix2 = {{2, 3, 1, 1}, {5, 6, 0, 1}, {3, 7, 5, 9}};
    int[][] finalMatrix = {{45, 70, 24, 45}, {55, 87, 31, 62}, {67, 109, 43, 77}, {12, 15, 1, 3}};
    assertArrayEquals(finalMatrix, Operations.multiplyMatrix(matrix1, matrix2));
  }

  @Test void erroneousMatrixMultiplyTest() {
    int[][] matrix1 = {{4, 5, 2}, {1, 7, 8}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}, {1, 5, 0}, {8, 6, 9}};
    assertNull(Operations.multiplyMatrix(matrix1, matrix2));
  }

  @Test void emptyMatrixMultiplyTest() {
    int[][] matrix1 = {{},{}};
    int[][] matrix2 = {{},{}};
    assertNull(Operations.multiplyMatrix(matrix1, matrix2));
  }
  @Test void emptyMatrixMultiplyTest2() {
    int[][] matrix1 = {};
    int[][] matrix2 = {};
    assertNull(Operations.multiplyMatrix(matrix1, matrix2));
  }
//==============================================================================
  @Test void sumMatrixTest_2x3() {
    int[][] matrix1 = {{4, 5}, {1, 7}, {8, 6}};
    int[][] matrix2 = {{2, 3}, {5, 6}, {3, 7}};
    int[][] finalMatrix = {{6, 8}, {6, 13}, {11, 13}};
    assertArrayEquals(finalMatrix, Operations.sumMatrix(matrix1, matrix2));
  }

  @Test void sumMatrixTest_3x4() {
    int[][] matrix1 = {{4, 5, 4}, {1, 7, 6}, {8, 6, 7}, {1, 2, 0}};
    int[][] matrix2 = {{2, 3, 1}, {5, 6, 0}, {3, 7, 5}, {1, 1, 9}};
    int[][] finalMatrix = {{6, 8, 5}, {6, 13, 6}, {11, 13, 12}, {2, 3, 9}};
    assertArrayEquals(finalMatrix, Operations.sumMatrix(matrix1, matrix2));
  }

  @Test void erroneousSumMatrixTest() {
    int[][] matrix1 = {{4, 5, 2}, {1, 7, 8}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}, {1, 5, 0}, {8, 6, 9}};
    assertNull(Operations.sumMatrix(matrix1, matrix2));
  }

  @Test void emptySumMatrixTest() {
    int[][] matrix1 = {{},{}};
    int[][] matrix2 = {{},{}};
    assertNull(Operations.sumMatrix(matrix1, matrix2));
  }

  @Test void emptySumMatrixTest2() {
    int[][] matrix1 = {};
    int[][] matrix2 = {};
    assertNull(Operations.sumMatrix(matrix1, matrix2));
  }
//=============================================================================
  @Test void deductionMatrixTest_2x3() {
    int[][] matrix1 = {{4, 5}, {1, 7}, {8, 6}};
    int[][] matrix2 = {{2, 3}, {5, 6}, {3, 7}};
    int[][] finalMatrix = {{2, 2}, {-4, 1}, {5, -1}};
    assertArrayEquals(finalMatrix, Operations.deductionMatrix(matrix1, matrix2));
  }

  @Test void deductionMatrixTest_3x4() {
    int[][] matrix1 = {{4, 5, 4}, {1, 7, 6}, {8, 6, 7}, {1, 2, 0}};
    int[][] matrix2 = {{2, 3, 1}, {5, 6, 0}, {3, 7, 5}, {1, 1, 9}};
    int[][] finalMatrix = {{2, 2, 3}, {-4, 1, 6}, {5, -1, 2}, {0, 1, -9}};
    assertArrayEquals(finalMatrix, Operations.deductionMatrix(matrix1, matrix2));
  }

  @Test void erroneousDeductionMatrixTest() {
    int[][] matrix1 = {{4, 5, 2}, {1, 7, 8}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}, {1, 5, 0}, {8, 6, 9}};
    assertNull(Operations.deductionMatrix(matrix1, matrix2));
  }

  @Test void emptyDeductionMatrixTest() {
    int[][] matrix1 = {{},{}};
    int[][] matrix2 = {{},{}};
    assertNull(Operations.deductionMatrix(matrix1, matrix2));
  }

  @Test void emptyDeductionMatrixTest2() {
    int[][] matrix1 = {};
    int[][] matrix2 = {};
    assertNull(Operations.sumMatrix(matrix1, matrix2));
  }
}
