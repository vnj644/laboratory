// Сделать программу сортировки массива
class Sorting {
  public static void main(String[] args) {
    int[] s = {1, 0, 2, 9, 3, 8, 4, 7, 5, 6};
    int i = 0;
    while (i < s.length-1) {
      if (s[i] < s[i + 1]) {
        int k = s[i];
        s[i] = s[i + 1];
        s[i + 1] = k;
        i = 0;
      }
      else {
        i++;
      }
    }
    for (i = 0; i < s.length; i++) {
      System.out.println(s[i]);
    }
  }
}
